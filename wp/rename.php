<?php
/*
Plugin Name: Rename Service List
Description: Переименование сервисных листов по их штрихкодам
Version: 1.0
Author: Плоткин Владислав
Author URI: 
Plugin URI: 
*/
function load_js(){
//	wp_enqueue_script('upload-js', WP_PLUGIN_URL.'/rename/upload.js',array('jquery'));
	wp_enqueue_script('bootstrap-js', WP_PLUGIN_URL.'/rename/bootstrap/js/bootstrap.min.js',array('jquery'));
        wp_enqueue_style('bootstrap-css', WP_PLUGIN_URL.'/rename/bootstrap/css/bootstrap.css');
	wp_localize_script( 'bootstrap-js', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));

}
add_action('wp_enqueue_scripts', 'load_js');
function rename_code( $atts ) {
    ?>
		<p>К переименованию допускаются сканы разрешением не менее 200dpi, многостраничники не допускаются.</p>
		<form enctype="multipart/form-data" action="/#" method="POST" class="mainform">
			<!-- Поле MAX_FILE_SIZE должно быть указано до поля загрузки файла -->
			<!--<input type="hidden" name="MAX_FILE_SIZE" value="3000000" />-->
			<!-- Название элемента input определяет имя в массиве $_FILES -->
			Отправить этот файл: <input type="file" id="file"  multiple="multiple" >
			<input type="button" class="submit button" value="Загрузить файлы">
			<div class="link_zip"></div>
			 <div class="progress">
			  <div class="progress-bar" role="progressbar"  aria-valuemin="0" aria-valuemax="100" style="width:0%">
			  </div>
			</div> 
<div class="filename" style="overflow:auto; height:200px;" ></div>
		</form>
	<script>
        (function($){
        // Автор: Тимур Камаев, http://wp-kama.ru/
        // Глобальная переменная куда будут располагаться данные файлов. С не будем работать
        var files;
		var progress = 0
		var max_progress
		var quant
        // Вешаем функцию на событие
        // Получим данные файлов и добавим их в переменную
        $('input[type=file]').change(function(){
	        files = this.files;
			max_progress = this.files.length
			quant = 100/max_progress
        });
        // Вешаем функцию ан событие click и отправляем AJAX запрос с данными файлов
        $('.submit.button').click(function( event ){
			zip_name = Math.random().toString(36)
	        event.stopPropagation(); // Остановка происходящего
	        event.preventDefault();  // Полная остановка происходящего

	        // Содадим данные формы и добавим в них данные файлов из files
	        var data = new FormData();
			i = 0
	        $.each( files, function( key, value ){
			   var data = new FormData();
 		       data.append( key, value );
			data.append('zipname',zip_name)
            data.append('action','upload_my_file')
	        // Отправляем запрос
			$.ajaxSetup({async: false});
	        $.ajax({
		        url: myAjax.ajaxurl+'?uploadfiles',
		        type: 'POST',
		        data: data,
		        cache: false,
		        dataType: 'json',
		        processData: false, // Не обрабатываем файлы (Don't process the files)
		        contentType: false, // Так jQuery скажет серверу что это строковой запрос
		        success: function( respond, textStatus, jqXHR ){
			        // Если все ОК
			        if( typeof respond.error === 'undefined' ){
						progress = progress+quant
						$(".progress-bar").css("width",progress+"%")
						$(".progress-bar").html(Math.round(progress)+'%')
						$(".filename").append(respond.files[0]+'<br>')
						//$(".filename").scrollTop = 9999
						//colsole.log(respond.files[0])
			        }
			        else{
				        console.log('ОШИБКИ ОТВЕТА сервера: ' + respond.error );
			        }
		        },
		        error: function( jqXHR, textStatus, errorThrown ){
			        console.log('ОШИБКИ AJAX запроса: ' + textStatus );
		        }
	        });
			});
			$('#file').val('')
			$('.progress-bar').css("width",'0%')
			progress = 0
			$('.filename').html('')
			$('.link_zip').append('<a id="'+zip_name+'" class=download href='+myAjax.ajaxurl+'?action=get_zip&zipname='+zip_name+'.zip ;>Скачать архив</br></a>');
			$('.download').click(function(){
				$(this).remove()
			})
        });

        })(jQuery)
        </script>




	<?php
}
require_once( ABSPATH . "wp-includes/pluggable.php" );
if ( is_user_logged_in() ) {
	add_shortcode( 'rename_sl', 'rename_code' );
}
else {
	add_shortcode( 'rename_sl', 'rename_error' );
}
add_action("wp_ajax_upload_my_file", "upload_my_file");

function upload_my_file(){
// Автор: Тимур Камаев, http://wp-kama.ru/
// Здесь нужно сделать все проверки передавемых файлов и вывести ошибки если нужно
// Переменная ответа
	$data = array();
	if( isset( $_GET['uploadfiles'] ) ){
		if (isset($_POST['zipname'])) {
			$zip_name = $_POST['zipname'];
		}  
		$error = false;
		$files = array();
		foreach( $_FILES as $key => $file){
            if( move_uploaded_file( $file['tmp_name'], $file['tmp_name'] ) ){
				$output = shell_exec('/var/www/html/wp-content/plugins/rename/rename.sh '.$file['tmp_name']);
				$output =  str_replace('pdf','pdf',str_replace("\n",'',($output == "error\n")?'/tmp/'.$file['name']:'/tmp/'.$output));
		        	$files[] = str_replace("/tmp/",'',$output);
				rename($file['tmp_name'], $output);
				shell_exec('zip -q -m "/tmp/'.$zip_name.'.zip" "'.$output.'"');
		    }
		    else{
		        $error = true;
			}
		}
		$data = $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $files );
		echo json_encode( $data );
		die();
	}
}

add_action("wp_ajax_get_zip", "get_zip");
function get_zip(){
	if(isset( $_GET['zipname'])){
		$zip_name = '/tmp/'.$_GET['zipname'];
		if(file_exists($zip_name))
			{
				header('Content-type: application/zip');
				header('Content-Disposition: attachment; filename="SL_'.date('d.m.Y_H:i').'.zip"');
				readfile($zip_name);
				unlink($zip_name);
			}

	}
}



function rename_error(){
	?>
	Вы не авторизованы
	<?php
}
